#include <SoftwareSerial.h>
#include <FastLED.h>


SoftwareSerial mySerial(10, 11); // RX, TX
#define NUM_STRIPS 1
#define NUM_LEDS_PER_STRIP 120
#define HUE 222
CRGB leds[NUM_STRIPS][NUM_LEDS_PER_STRIP];

int currentStrip = -1;
int currentLED = -1;

void setup() {
  Serial.begin(9600);

  Serial.println("Goodnight moon!");

  // set the baud rate for the SoftwareSerial port
  mySerial.begin(38400);
  mySerial.listen();

  // initialisation des rubans de LED
  FastLED.addLeds<NEOPIXEL, 2>(leds[0], NUM_LEDS_PER_STRIP);
  //FastLED.addLeds<NEOPIXEL, 3>(leds[1], NUM_LEDS_PER_STRIP);

  mySerial.println("Talisman is ready !");
  turnOff();

}

void loop() { // run over and over
  if (mySerial.available()) {
    Serial.println("Got something!");
    char command = mySerial.read();
    Serial.println(command);
    if ((int)command >= 48 && (int)command <= 57) {
      int strip = (int)command - 48;
      Serial.println(strip);

      if (strip != currentStrip) {
        turnOff();
        currentLED = 0;
        currentStrip = strip;
      }
    } else {
      //skip
    }

  }
  Serial.print("Current strip : ");
  Serial.println(currentStrip);
  Serial.print("Current LED : ");
  Serial.println(currentLED);
  moveLED();
  delay(10);
}

void moveLED() {
  if (currentLED == -1) {
    return;
  }

  leds[currentStrip][currentLED] = CHSV( HUE, 60, 60);
  leds[currentStrip][currentLED+1] = CHSV( HUE, 180, 180);
  leds[currentStrip][currentLED+2] = CHSV( HUE, 255, 255);
  leds[currentStrip][currentLED+3] = CHSV( HUE, 180, 180);
  leds[currentStrip][currentLED+4] = CHSV( HUE, 60, 60);
  if (currentLED > 0) {
    leds[currentStrip][currentLED-1] = CRGB::Black;
  }
  FastLED.show();
  currentLED++;

  if (currentLED == NUM_LEDS_PER_STRIP -5) {
    currentLED = 0;
  }
}

void turnOff() {
  currentLED = -1;
  FastLED.clear();  // clear all pixel data
  FastLED.show();
}

