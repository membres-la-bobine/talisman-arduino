/* Projet Talisman
  * Une petite appli pour gérer un (des) ruban(s) de leds
  *
  * @author 2020 Jean-Louis Frucot <frucot.jeanlouis@free.fr>
  * @see The GNU Public License (GPL)
  *
  * this program is free software you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY
  * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  * for more details.
  *
  * You should have received a copy of the GNU General Public License along
  * with this program. If not, see <http://www.gnu.org/licenses/>.
  */

#include <Arduino.h>
#include <FastLED.h>

#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>

////////////////////////////// Mes inclusions   /////////////////////////////////////////////
#include "settings.h"
#include "rubans.h"
#include "com.h"
#include "JLF_Timer.h"

///////////////////////////// Initialisations des variables  /////////////////////////////////
char octetReception = 0;     // variable de stockage des valeurs reçues sur le port Série (ASCII)
String chaineReception = ""; // déclare un objet String vide pour reception chaine

AsyncWebServer server(SERVER_PORT); // Serveur sur le port defini dans settings.h
// Création d'un timer pour rythmer l'affichage des leds
JLF_Timer *m_stripTimer = new JLF_Timer(m_stepDuration); // ms

bool animationsEnd = true;
bool batterieOn = true;
bool batterieModeHasChanged = true;
bool passagerOn = false;
bool passagerHasChanged = true;
uint8_t nb_boucles = 0;
void setup()
{
  Serial.begin(9600);
  delay(5000); // On attend que le serveur du deuxième ESP32 soit démarré
#if OUTPUT_SERIAL_PRINT
  Serial.print("Connexion à ");
  Serial.println(ssid);
#endif
  startWifi(); // Connect to Wi-Fi network with SSID and password

  // initialisation des rubans
  initStrips();
  createFastLedsStrips();
  initSegmentsParam();
  initSegments();

  // Création de la liaison série entre les deux microcontroleurs
  m_serial.begin(9600, SERIAL_8N1, RXPIN, TXPIN);
  delay(200); // On laisse un peu de temps pour que les choses se mettent en place

  // Route pour " /" web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(200, "text/plain", "Hello, Vous êtes sur le serveur ESP32-Rubans");
  });
  AsyncElegantOTA.begin(&server); // Start ElegantOTA à l'adresse /update/
  server.begin();

  // C'est parti !
  FastLED.clear(true);
  FastLED.show();

  stopWifi(); // On arrête le wifi pour des économies d'énergie
  listAnims.clear();
  m_stripTimer->start(); // C'est parti !

}

void loop()
{
  if (WiFi.status() == WL_CONNECTED)
  {
    AsyncElegantOTA.loop(); // On guette une demande de mise à jour du firmware
  }

  ////////////////////  Gestion du port série    ////////////////////////////
  while (m_serial.available() > 0)
  {
    chaineReception = m_serial.readStringUntil('>'); // Le '>' n'est pas compris dans la chaine de retour
#if OUTPUT_SERIAL_PRINT
    Serial.print("On vient de recevoir : ");
    Serial.println(chaineReception);
#endif
    chaineReception = chaineReception.substring(chaineReception.indexOf("<") + 1); // On enlève le "<" de la chaine
#if OUTPUT_SERIAL_PRINT
    Serial.print("On vient de recevoir : ");
    Serial.println(chaineReception);
#endif
    int8_t indexSeparateur = chaineReception.indexOf(":"); // On repère la place du ":"
    if (indexSeparateur == -1)
    {
      break;
    }

    uint8_t m_ruban = chaineReception.substring(0, indexSeparateur).toInt();     // On extrait le numéro du ruban
    uint8_t m_commande = chaineReception.substring(indexSeparateur + 1).toInt(); // On extrait le numéro de la commande
    
    #if OUTPUT_SERIAL_PRINT
    Serial.print("Segment N°");
    Serial.print(m_ruban);
    Serial.print(" Commande N°");
    Serial.println(m_commande);
  #endif
    if (m_ruban >= 12) {
      Serial.println("bad ruban, skipping.");
      continue;
    }
    commandeStrip(m_ruban, m_commande);                                          // Lancer la commande


  }
  // Gestion du ruban de la batterie
  if (batterieModeHasChanged)
  {
    if (batterieOn)
    {
      // On allume le ruban de la batterie
      fill_solid(listRubans[0], Tab_NB_LEDS[0], CHSV(HUE_BLUE, 255, 64));
      Serial.println("batterie On");
    }
    else
    {
      // On éteint le ruban de la batterie
      fill_solid(listRubans[0], Tab_NB_LEDS[0], CHSV(HUE_BLUE, 255, 0));
      Serial.println("batterie Off");
    }
    batterieModeHasChanged = false;
    FastLED.show();
  }

  // Gestion du ruban du siège passager
  /*if (passagerHasChanged)
  {
    if (passagerOn)
    {
      // On allume le ruban du siège passager
      fill_solid(listRubans[1], Tab_NB_LEDS[1], CHSV(HUE_BLUE, 255, 64));
      Serial.println("Passager On");
    }
    else
    {
      // On éteint le ruban du siège passager
      fill_solid(listRubans[1], Tab_NB_LEDS[1], CHSV(HUE_BLUE, 255, 0));
      Serial.println("Passager Off");
    }
    passagerHasChanged = false;
    FastLED.show();
  }*/
  // ici, on gère l'affichage à chaque fois que le timer a basculé
  // et que la batterie n'est pas déconnectée
  if (m_stripTimer->isTimeElapsed() && batterieOn == true)
  {
    // Tant qu'on est pas arrivé à la fin des animations, on continue
    if (animationsEnd == false)
    {
#if OUTPUT_SERIAL_PRINT
      for (it_ListAnims = listAnims.begin(); it_ListAnims != listAnims.end(); ++it_ListAnims)
      {
        /*Serial.print("Iterateur : ");
        Serial.print(*it_ListAnims);
        Serial.println("   ");
        Serial.print("Taille : ");
        Serial.println(listAnims.size());*/
      }

#endif
      if (!listAnims.empty())
      {
        for (it_ListAnims = listAnims.begin(); it_ListAnims != listAnims.end();)
        {
#if OUTPUT_SERIAL_PRINT
          Serial.print("Itérateur : ");
          Serial.println(*it_ListAnims);
#endif
          if (listSegments[*it_ListAnims]->animationHasEnded())
          {
#if OUTPUT_SERIAL_PRINT
            Serial.println("Animation terminée");
#endif
            it_ListAnims = listAnims.erase(it_ListAnims); // On enlève l'anim de la liste
          }
          else
          {
            ++it_ListAnims;
          }
        }
        if (listAnims.empty())
        {
#if OUTPUT_SERIAL_PRINT
          Serial.println("listAnims vide");
#endif
          writeToSerial(90); // On informe l'autre ESP que l'anim est terminée
        }
      }
    }
    // On passe en revue tous les segments qui sont dans la liste des animations du scenario
    for (it_ListAnims = listAnims.begin(); it_ListAnims != listAnims.end(); ++it_ListAnims)
    {
      if (listSegments[*it_ListAnims]->IsRunning()) // si le segment a une animation en cours
      {
        listSegments[*it_ListAnims]->updateAnimation(); // On la met à jour
      }
    }
    FastLED.show();
  }
}
void commandeStrip(uint8_t numSegments, uint8_t action)
{
  #if OUTPUT_SERIAL_PRINT
      Serial.println("commandeStrip : " + String(numSegments));
  #endif
  if (numSegments < SEGMENTS_COUNT && batterieOn)
  {
    switch (action)
    {
      // On lance l'action correspondant au code reçu
    case Stop:
      //stopStrip(numSegments);
      // clearStrip(numSegments);
      FastLED.show();
#if OUTPUT_SERIAL_PRINT
      Serial.println("ClearStrip : " + String(numSegments));
#endif
      break;
    case Start:

      listAnims.push_back(numSegments);

      listSegments[numSegments]->start();
      animationsEnd = false;
#if OUTPUT_SERIAL_PRINT
      Serial.println("Starts Segment : " + String(numSegments));
#endif
      break;
    case Resume:
      // resumeStrip(numSegments);
#if OUTPUT_SERIAL_PRINT
      Serial.println("resumeStrip : " + String(numSegments));
#endif
      break;
    default:
      // On ne fait rien
      break;
    }
  }
  else
  {
    switch (numSegments)
    {
    case 80: // Gestion du ruban de batterie
      if (action == 1 && batterieOn != true)
      {
        batterieModeHasChanged = true;
        batterieOn = true;
      }
      if (action == 0 && batterieOn != false)
      {
        batterieModeHasChanged = true;
        batterieOn = false;
      }
      break;
    case 85: // Gestion du ruban de batterie
      if (action == 1 && passagerOn != true)
      {
        passagerHasChanged = true;
        passagerOn = true;
      }
      if (action == 0 && passagerOn != false)
      {
        passagerHasChanged = true;
        passagerOn = false;
      }
      break;
    case 99:
      clearAllSegments();
      break;
    case 98:
      if (action > 0)
      {
        startWifi();
      }
      else
      {
        stopWifi();
      }
      break;
    default:
      break;
    }
  }
}