#ifndef HEADERS_RUBAN_H
#define HEADERS_RUBAN_H
#include <AsyncElegantOTA.h>
#include "settings.h"


void writeToSerial(uint8_t numSegment);
void startWifi();
void stopWifi();
void commandeStrip(uint8_t numStrip, uint8_t action);


#endif //HEADERS_RUBAN_H