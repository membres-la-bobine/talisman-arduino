#ifndef SETTINGS1_H
#define SETTINGS1_H

#include <Arduino.h>
#include <FastLED.h>
#include <list>
using namespace std;
#define OUTPUT_SERIAL_PRINT 1 // 1 affiche les debugs, 0 ne les affiche pas

///////////////////   Le Wifi       ///////////////////////////////
const char *ssid = "talisman";      // Identifiant du réseau
const char *password = "123456789"; // Mot de passe réseau
IPAddress local(192, 168, 4, 10);   // L'adresse du microcontroleur des rubans
IPAddress gateway(192, 168, 4, 1);  // Adresse du l'autre ESP
IPAddress subnet(255, 255, 255, 0);
#define SERVER_PORT 8888

/////////////////// Liaison série    //////////////////////////////
const uint8_t RXPIN = 16;   // Broche RX pour le port série avec ESP32
const uint8_t TXPIN = 17;   // Broche TX pour le port série avec ESP32
HardwareSerial m_serial(1); // Port série ne pas modifier

//////////////////// Les LEDs        ///////////////////////////////
const uint8_t STRIP_COUNT = 10; // Le nombre de rubans
const uint16_t Tab_NB_LEDS[STRIP_COUNT] = {110, 110, 42, 170, 23, 110, 110, 110, 120, 110}; // Le nombre de leds de chaque ruban

#define RUBAN_00_PIN 25 // batterie ok (soudure à isoler)
#define RUBAN_01_PIN 26 // capteur siege passager ou montant porte ok
#define RUBAN_02_PIN 32 // Capteur Frontal KO
#define RUBAN_03_PIN 33 // Airbag Rideau OK
#define RUBAN_04_PIN 18 // Pretensionneur Conducteur OK
#define RUBAN_05_PIN 19 // Airbag Conducteur OK
#define RUBAN_06_PIN 21 // Pretensionneur passager OK
#define RUBAN_07_PIN 22 // airbag passager OK
#define RUBAN_08_PIN 23 // porte sensor OK
#define RUBAN_09_PIN 23 // WHO AM I ?


//////////////////// Les couleurs    ///////////////////////////////
const uint8_t COLORS_COUNT = 3;
enum colors : uint8_t // Les noms des couleurs
{
  rouge,
  bleu,
  vert
};
CHSV Tab_COLORS[COLORS_COUNT] = {CHSV(HUE_RED, 255, 255),
                                 CHSV(HUE_BLUE, 255, 255),
                                 CHSV(HUE_GREEN, 255, 255)}; // Les couleurs utilisées

CRGB *listMotifs[COLORS_COUNT]; // Pointeur vers la liste des motifs
const uint8_t LIGHTNING_LEDS_COUNT = 8;

///////////////////// Les segments    ///////////////////////////////
enum segmentType : uint8_t
{
  none,
  sensor,
  line
};
enum Direction : uint8_t
{
  inverse = 0,
  direct
};

const uint8_t SEGMENTS_COUNT = 16;

struct segment
{
  uint8_t segmentType = none;                 /**< none, sensor, line... */
  uint8_t strip;                              /**< Numéro du ruban du ruban */
  uint16_t firstLed;                          /**< La première led du segment sur le ruban */
  uint16_t nb_leds;                           /**< Le nombre de leds du segment */
  uint16_t nb_steps;                          /**< Nombre de pas de l'animation */
  int8_t nb_loops = 1;                        /**< Nombre de boucles à parcourir -1 pas de limite */
  CRGB *motif;                                /**< Le motif de l'animation pour le type "ledsLines" (il est dans listMotif[]) */
  uint8_t motifLength = LIGHTNING_LEDS_COUNT; /** La longueur du motif */
  CHSV color;                                 /**< La couleur utilisée par le type "sensor" */
  bool direction = direct;                    /**< Le sens de l'animation : direct sens normal, reverse sens inverse */
};

segment Tab_Segments[SEGMENTS_COUNT]; // La liste des segments : il est judicieux d'utiliser enum SegmentName pour les indices
list<uint8_t> listAnims;              // Pour stocker les segments utilisés par les animations
list<uint8_t>::iterator it_ListAnims;
enum SegmentName : uint8_t
{
  frontalsensor,
  portesensor,
  montantsensor,
  montantline,
  pretensconducteur,
  pretenspassager,
  pretenspassagerline,
  airbagconducteur,
  airbagpassager,
  airbagpassagerline,
  airbagrideauline,
  airbagrideau,
  unitecentrale
};

/** \brief Initialise les segments avec leurs paramètres
 */
void initSegmentsParam()
{
  for (uint8_t i = 0; i < SEGMENTS_COUNT; i++)
  {
    Tab_Segments[i].segmentType = none;
  }

  // Le détecteur de chocs frontal
  Tab_Segments[frontalsensor].segmentType = sensor;
  Tab_Segments[frontalsensor].strip = 2;
  Tab_Segments[frontalsensor].firstLed = 0;
  Tab_Segments[frontalsensor].nb_leds = 42;
  Tab_Segments[frontalsensor].nb_loops = 5;
  Tab_Segments[frontalsensor].nb_steps = 20;
  Tab_Segments[frontalsensor].color = CHSV(HUE_RED, 255, 255);

  // La liaison UCE vers airbag rideau
  Tab_Segments[airbagrideauline].segmentType = line;
  Tab_Segments[airbagrideauline].strip = 3;
  Tab_Segments[airbagrideauline].firstLed = 0;
  Tab_Segments[airbagrideauline].nb_leds = 160;
  Tab_Segments[airbagrideauline].nb_loops = 1;
  Tab_Segments[airbagrideauline].nb_steps = 160;
  Tab_Segments[airbagrideauline].motif = listMotifs[vert];
  Tab_Segments[airbagrideauline].direction = direct;

  // airbag rideau
  Tab_Segments[airbagrideau].segmentType = sensor;
  Tab_Segments[airbagrideau].strip = 3;
  Tab_Segments[airbagrideau].firstLed = 160;
  Tab_Segments[airbagrideau].nb_leds = 10;
  Tab_Segments[airbagrideau].nb_loops = 3;
  Tab_Segments[airbagrideau].nb_steps = 20;
  Tab_Segments[airbagrideau].color = CHSV(HUE_GREEN, 255, 255);

  // Prétensionneur Conducteur
  Tab_Segments[pretensconducteur].segmentType = sensor;
  Tab_Segments[pretensconducteur].strip = 4;
  Tab_Segments[pretensconducteur].firstLed = 0;
  Tab_Segments[pretensconducteur].nb_leds = 17;
  Tab_Segments[pretensconducteur].nb_loops = 5;
  Tab_Segments[pretensconducteur].nb_steps = 20;
  Tab_Segments[pretensconducteur].color = CHSV(HUE_GREEN, 255, 255); 

  // AirBag Conducteur
  Tab_Segments[airbagconducteur].segmentType = sensor;
  Tab_Segments[airbagconducteur].strip = 5;
  Tab_Segments[airbagconducteur].firstLed = 0;
  Tab_Segments[airbagconducteur].nb_leds = 12;
  Tab_Segments[airbagconducteur].nb_loops = 5;
  Tab_Segments[airbagconducteur].nb_steps = 20;
  Tab_Segments[airbagconducteur].color = CHSV(HUE_GREEN, 255, 255); 
  
  // Prétensionneur Passager
  Tab_Segments[pretenspassager].segmentType = sensor;
  Tab_Segments[pretenspassager].strip = 6;
  Tab_Segments[pretenspassager].firstLed = 170;
  Tab_Segments[pretenspassager].nb_leds = 23;
  Tab_Segments[pretenspassager].nb_loops = 3;
  Tab_Segments[pretenspassager].nb_steps = 20;
  Tab_Segments[pretenspassager].color = CHSV(HUE_GREEN, 255, 255); 
  
  // UC vers Prétensionneur Passager
  Tab_Segments[pretenspassagerline].segmentType = line;
  Tab_Segments[pretenspassagerline].strip = 6;
  Tab_Segments[pretenspassagerline].firstLed = 0;
  Tab_Segments[pretenspassagerline].nb_leds = 170;
  Tab_Segments[pretenspassagerline].nb_loops = 1;
  Tab_Segments[pretenspassagerline].nb_steps = 170;
  Tab_Segments[pretenspassagerline].motif = listMotifs[vert];
  Tab_Segments[pretenspassagerline].direction = direct;

  // AirBag Passager
  Tab_Segments[airbagpassager].segmentType = sensor;
  Tab_Segments[airbagpassager].strip = 7;
  Tab_Segments[airbagpassager].firstLed = 83;
  Tab_Segments[airbagpassager].nb_leds = 26;
  Tab_Segments[airbagpassager].nb_loops = 3;
  Tab_Segments[airbagpassager].nb_steps = 20;
  Tab_Segments[airbagpassager].color = CHSV(HUE_GREEN, 255, 255); 
  
  // UC vers AirBag Passager
  Tab_Segments[airbagpassagerline].segmentType = line;
  Tab_Segments[airbagpassagerline].strip = 7;
  Tab_Segments[airbagpassagerline].firstLed = 0;
  Tab_Segments[airbagpassagerline].nb_leds = 84;
  Tab_Segments[airbagpassagerline].nb_loops = 1;
  Tab_Segments[airbagpassagerline].nb_steps = 84;
  Tab_Segments[airbagpassagerline].motif = listMotifs[vert];
  Tab_Segments[airbagpassagerline].direction = direct;

  // Détecteur porte
  Tab_Segments[portesensor].segmentType = sensor;
  Tab_Segments[portesensor].strip = 8;
  Tab_Segments[portesensor].firstLed = 0;
  Tab_Segments[portesensor].nb_leds = 23;
  Tab_Segments[portesensor].nb_loops = 3;
  Tab_Segments[portesensor].nb_steps = 20;
  Tab_Segments[portesensor].color = CHSV(HUE_RED, 255, 255); 

  // Détecteur Montant
  Tab_Segments[montantsensor].segmentType = sensor;
  Tab_Segments[montantsensor].strip = 1;
  Tab_Segments[montantsensor].firstLed = 82;
  Tab_Segments[montantsensor].nb_leds = 6;
  Tab_Segments[montantsensor].nb_loops = 3;
  Tab_Segments[montantsensor].nb_steps = 20;
  Tab_Segments[montantsensor].color = CHSV(HUE_RED, 255, 255); 

  // Détecteur Montant vers UC
  Tab_Segments[montantline].segmentType = line;
  Tab_Segments[montantline].strip = 1;
  Tab_Segments[montantline].firstLed = 0;
  Tab_Segments[montantline].nb_leds = 82;
  Tab_Segments[montantline].nb_loops = 1;
  Tab_Segments[montantline].nb_steps = 84;
  Tab_Segments[montantline].motif = listMotifs[rouge];
  Tab_Segments[montantline].direction = inverse;
}
                         
// ms : règle la vitesse d'affichage des Leds
uint16_t m_stepDuration = 25; 

#endif // SETTINGS_H