#ifndef LEDSLINES_H
#define LEDSLINES_H

#include "segments.h"

class LedsLine : public Segments
{
public:
    LedsLine(CRGB *pStrip, uint16_t numFirstLed, uint16_t nbLeds, const uint16_t nbSteps, const uint16_t nbLoops, CRGB *motif, uint16_t motifLength, bool direction = true);
    /** \brief Démarre l'animation du segment concerné.
     */
    virtual void start();

    /** \brief Fonction qui fait passer l'animation dans l'état suivant.
     */
    virtual void updateAnimation();

    ~LedsLine();

private:
    CRGB *m_motif;
    uint16_t m_motifLength;
    bool m_direction;
};

#endif //LEDSLINES_H
