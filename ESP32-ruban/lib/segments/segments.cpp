
#include "segments.h"

Segments::Segments(CRGB *pStrip, const uint16_t numFirstLed, uint16_t nbLeds, const uint16_t nbSteps, const uint16_t nbLoops) : 
                                                                                m_pStrip(pStrip),
                                                                                m_numFirstLed(numFirstLed),
                                                                                m_nbLeds(nbLeds),
                                                                                m_nbSteps(nbSteps),
                                                                                m_nbLoops(nbLoops)
{
    m_bIsRunning = false;
}
Segments::~Segments()
{
}

void Segments::start()
{
    
    // On démarre l'animation du ruban
    m_bIsRunning = true;
    m_currentLoop = 0;
    m_currentStep = 0;
}
bool Segments::IsRunning()
{
    return m_bIsRunning;
}

bool Segments::animationHasEnded()
{
    bool ended = m_ended;
    if(m_ended)
    {
        m_ended = false;
    }
    return ended;
}

void Segments::clearSegment()
{
    // On remplit le buffer du segment avec du Noir
    fill_solid(m_pStrip + m_numFirstLed, m_nbLeds, CRGB::Black);
}
