#ifndef SEGMENTS_H
#define SEGMENTS_H

#include <Arduino.h>
#include <FastLED.h>

class Segments
{
public:
    Segments(CRGB *pStrip, uint16_t numFirstLed, uint16_t nbLeds, const uint16_t nbSteps, const uint16_t nbLoops);

    ~Segments();
    /** \brief Met à jour l'animation.
     */
    virtual void updateAnimation() = 0;
    /** Démarre l'animation.
     */
    virtual void start();
    /** \brief Retourne l'état de l'animation
     * 
     * \return Arrêtée (0) ou En cours (1)
     */
    bool IsRunning();
    /** L'animation vient de se terminer
     * 
     * \return Oui (true) Non (false)
     */
    virtual bool animationHasEnded();
    /** \brief Met les diodes du segment à Black (éteintes)
     */
    void clearSegment();

protected:

CRGB *m_pStrip = 0;
const uint16_t m_numFirstLed;
const uint16_t m_nbLeds;
bool m_bIsRunning;
bool m_ended = false;
const uint16_t m_nbSteps;
uint16_t m_currentStep;
const uint16_t m_nbLoops;
uint16_t m_currentLoop;
CRGB *m_pLeds = 0;
};

#endif // SEGMENTS_H