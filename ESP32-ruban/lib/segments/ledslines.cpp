
#include "ledslines.h"

LedsLine::LedsLine(CRGB *pStrip,
                   uint16_t numFirstLed,
                   uint16_t nbLeds,
                   const uint16_t nbSteps,
                   const uint16_t nbLoops,
                   CRGB *motif,
                   uint16_t motifLength,
                   bool direction) : Segments(pStrip, numFirstLed, nbLeds, nbSteps, nbLoops),
                                     m_motif(motif),
                                     m_motifLength(motifLength),
                                     m_direction(direction)
{
    if (m_direction == false)
    {
        // On inverse les couleurs du motif
        for (uint16_t k = 0, j = m_motifLength - 1; k < j; k++, j--) // Syntaxe inusitée, mais pratique !
        {
            CRGB temp = m_motif[k];
            m_motif[k] = m_motif[j];
            m_motif[j] = temp;
        }
    }
}
LedsLine::~LedsLine()
{
}
void LedsLine::start()
{
    // Si on a des trucs particuliers à faire, il faut les faire avant cette ligne
    Segments::start();
    m_pLeds = new CRGB[m_nbLeds + (m_motifLength - 1) * 2];
}
void LedsLine::updateAnimation()
{
    if (m_bIsRunning)
    {
        // Création d'un buffer de leds qui sera copié dans le buffer du ruban
        fill_solid(m_pLeds, m_nbLeds + (m_motifLength - 1) * 2, CRGB::Black);
        if (m_currentLoop < m_nbLoops)
        {
            // On passe à la suite de l'animation
            if (m_currentStep < m_nbSteps + m_motifLength)
            {
                // On copie le motif dans le buffer au bon endroit
                if (m_direction == true)
                {
                    memcpy(m_pLeds + m_currentStep, m_motif, m_motifLength * 3);
                }
                else
                {
                    // Le défilement se fait dans le sens inverse
                    memcpy(m_pLeds + m_nbLeds + m_motifLength - 2 - m_currentStep, m_motif, m_motifLength * 3);
                }

                // On copie le buffer calculé dans le buffer du ruban de leds
                memcpy(m_pStrip + m_numFirstLed, m_pLeds + m_motifLength, m_nbLeds * 3);
                m_currentStep++;
            }
            else
            {
                m_currentStep = 0;
                m_currentLoop++;
            }
        }
        else
        {
            m_bIsRunning = false;

            clearSegment();
            // On informe de la fin de l'animation
            m_ended = true;
            if (!m_pLeds)
            {
                delete[] m_pLeds;
                m_pLeds = 0;
            }
        }
    }
}