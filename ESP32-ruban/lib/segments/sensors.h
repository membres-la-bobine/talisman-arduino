#ifndef SENSORS_H
#define SENSORS_H

#include "segments.h"

class Sensor : public Segments
{
public:
    Sensor(CRGB *pStrip, uint16_t numFirstLed, uint16_t nbLeds, const uint16_t nbSteps, const uint16_t nbLoops, CHSV color);
    /** \brief Démarre l'animation du segment concerné.
     */
    void start();
    /** \brief Fonction qui fait passer l'animation dans l'état suivant.
     */
    virtual void updateAnimation();
    ~Sensor();

private:
    CRGB *m_pLeds;
    CHSV m_color;
    CHSV m_currentColor;
    uint8_t m_fadeFactor;
};

#endif //SENSORS_H
