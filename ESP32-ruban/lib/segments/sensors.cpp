
#include "sensors.h"

Sensor::Sensor(CRGB *pStrip, uint16_t numFirstLed, uint16_t nbLeds, const uint16_t nbSteps, const uint16_t nbLoops, CHSV color) : Segments(pStrip, numFirstLed, nbLeds, nbSteps, nbLoops),
                                                                                                                                  m_color(color)

{
    m_currentColor = m_color;
    m_fadeFactor = 255 / m_nbSteps;
}
Sensor::~Sensor()
{
}
void Sensor::start()
{
    // Si on a des trucs particuliers à faire, il faut les faire avant cette ligne
    m_pLeds = new CRGB[m_nbLeds];
    Segments::start();
}
void Sensor::updateAnimation()
{

    if (m_bIsRunning)
    {
        // On passe à la suite de l'animation
        if (m_currentLoop < m_nbLoops)
        {
            if (!m_nbLoops || m_currentStep < m_nbSteps)
            {
                m_currentColor.value = m_color.value - m_fadeFactor * m_currentStep;
                fill_solid(m_pLeds, m_nbLeds, m_currentColor);
                memcpy(m_pStrip + m_numFirstLed, m_pLeds, m_nbLeds * 3);
                m_currentStep++;
            }
            else
            {
                m_currentStep = 0;
                m_currentColor = m_color;
                if (m_nbLoops > 0) // si  0, l'animation ne s'arrête pas
                {
                    m_currentLoop++;
                }
            }
        }
        else // L'animation est terminée
        {
            m_bIsRunning = false;
            clearSegment();
            // On informe de la fin de l'animation
            m_ended = true;
            if (!m_pLeds)
            {
                delete[] m_pLeds;
                m_pLeds = 0;
            }
        }
    }
}
