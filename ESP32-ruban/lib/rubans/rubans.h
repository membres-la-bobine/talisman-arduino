#ifndef RUBANS_H
#define RUBANS_H

#include <Arduino.h>
#include <FastLED.h>
#include "headers.h"
#include "sensors.h"
#include "ledslines.h"

// #define OUTPUT_SERIAL_PRINT_DEBUG 0 // 1 affiche les debugs, 0 ne les affiche pas

CRGB *listRubans[STRIP_COUNT]; // Pointeur vers les buffer des rubans

Segments *listSegments[SEGMENTS_COUNT]; // Liste des segments (pointeurs)

enum StripState : uint8_t // L'état de l'animation arrêtée ou en marche
{
  stopped = 0,
  running = 1,
  paused = 2
};

enum Commande : uint8_t // commande envoyée au ruban concerné
{
  Stop = 0,
  Start = 1,
  Resume = 2
};

/** \brief Initialise les Rubans de leds avec les valeurs données dans settings.h
 * 
 * - Crée le buffer de leds 
 * - Calcule le motif des leds allumées lors de l'animation, la dernière étant noire
 **/
void initStrips()
{

  // Création du buffer de leds : longueur = nb de leds + 2 fois (la longueur du motif-1)
  for (uint8_t i = 0; i < STRIP_COUNT; i++)
  {
    listRubans[i] = new CRGB[Tab_NB_LEDS[i]];
  }

  // Remplissage du tableau des leds allumées (motif[n]) la première étant Black
  for (uint8_t i = 0; i < COLORS_COUNT; i++)
  {
    listMotifs[i] = new CRGB[LIGHTNING_LEDS_COUNT];
    listMotifs[i][0] = CRGB::Black;
    for (uint8_t j = LIGHTNING_LEDS_COUNT - 1; j > 0; j--)
    {
      CHSV color = Tab_COLORS[i];
      color.value = 255 - (j - 1) * 40; // Calcul à revoir (ou pas);
      listMotifs[i][8 - j] = color;
    }
  }
}
/** \brief Création des Rubans de leds.
 * 
 * Nécessaire pour l'utilisation de FastLED
 */
void createFastLedsStrips()
{
  // Une limitation de C++ liée aux templates empêche d'utiliser une variable pour le N° du pin
  // offset de LIGHTNINNG_LEDS_COUNT-1 leds comme cela la première led est affichée et les sept autres masquées
  // Les numéros des broches sont définis dans setings.h
  if (STRIP_COUNT > 0)
  {
    FastLED.addLeds<NEOPIXEL, RUBAN_00_PIN>(listRubans[0], Tab_NB_LEDS[0]);
  }
  if (STRIP_COUNT > 1)
  {
    FastLED.addLeds<NEOPIXEL, RUBAN_01_PIN>(listRubans[1], Tab_NB_LEDS[1]);
  }
  if (STRIP_COUNT > 2)
  {
    FastLED.addLeds<NEOPIXEL, RUBAN_02_PIN>(listRubans[2], Tab_NB_LEDS[2]);
  }
  if (STRIP_COUNT > 3)
  {
    FastLED.addLeds<NEOPIXEL, RUBAN_03_PIN>(listRubans[3], Tab_NB_LEDS[3]);
  }
  if (STRIP_COUNT > 4)
  {
    FastLED.addLeds<NEOPIXEL, RUBAN_04_PIN>(listRubans[4], Tab_NB_LEDS[4]);
  }
  if (STRIP_COUNT > 5)
  {
    FastLED.addLeds<NEOPIXEL, RUBAN_05_PIN>(listRubans[5], Tab_NB_LEDS[5]);
  }
  if (STRIP_COUNT > 6)
  {
    FastLED.addLeds<NEOPIXEL, RUBAN_06_PIN>(listRubans[6], Tab_NB_LEDS[6]);
  }
  if (STRIP_COUNT > 7)
  {
    FastLED.addLeds<NEOPIXEL, RUBAN_07_PIN>(listRubans[7], Tab_NB_LEDS[7]);
  }
  if (STRIP_COUNT > 8)
  {
    FastLED.addLeds<NEOPIXEL, RUBAN_08_PIN>(listRubans[8], Tab_NB_LEDS[8]);
  }
  if (STRIP_COUNT > 9)
  {
    FastLED.addLeds<NEOPIXEL, RUBAN_09_PIN>(listRubans[9], Tab_NB_LEDS[9]);
  }
}

void initSegments()
{
  for (uint8_t i = 0; i < SEGMENTS_COUNT; i++)
  {
    switch (Tab_Segments[i].segmentType)
    {
    case sensor:
      /* un segment de type sensor */
      listSegments[i] = new Sensor(listRubans[Tab_Segments[i].strip],
                                   Tab_Segments[i].firstLed,
                                   Tab_Segments[i].nb_leds,
                                   Tab_Segments[i].nb_steps,
                                   Tab_Segments[i].nb_loops,
                                   Tab_Segments[i].color);                           
      break;
    case line:
      /* un segment de type chenillard */
      listSegments[i] = new LedsLine(listRubans[Tab_Segments[i].strip],
                                     Tab_Segments[i].firstLed,
                                     Tab_Segments[i].nb_leds,
                                     Tab_Segments[i].nb_steps,
                                     Tab_Segments[i].nb_loops,
                                     Tab_Segments[i].motif,
                                     Tab_Segments[i].motifLength,
                                     Tab_Segments[i].direction);
      break;
    default:
      /* rien */
      listSegments[i] = nullptr;
      break;
    }
  }
}

/** \fn writeToSerial(uint8_t numStrip)
 * \brief Ecris sur le port m_serial un code de la forme <numStrip:numAction>
 * 
 * \param numStrip numéro du ruban concerné
 */
void writeToSerial(uint8_t numSegment)
{
  m_serial.write("<");
  m_serial.print(numSegment);
  m_serial.write(":");
  m_serial.print("0");
  m_serial.println(">");
}

void clearAllSegments()
{
  for (uint8_t i = 0; i < SEGMENTS_COUNT; i++)
  {
    if (listSegments[i] != nullptr)
    {
      listSegments[i]->clearSegment();
    }
  }
}

#endif //RUBANS_H