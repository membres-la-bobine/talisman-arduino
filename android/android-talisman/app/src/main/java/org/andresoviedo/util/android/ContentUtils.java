package org.andresoviedo.util.android;

import android.app.Activity;
import android.net.Uri;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class ContentUtils {

    /**
     * Documents opened by the user. This list helps finding the relative filenames found in the model
     */
    private static Map<String, Uri> documentsProvided = new HashMap<>();

    private static ThreadLocal<Activity> currentActivity = new ThreadLocal<>();

    private static File currentDir = null;

    public static void setThreadActivity(Activity currentActivity) {
        Log.i("ContentUtils", "Current activity thread: " + Thread.currentThread().getName());
        ContentUtils.currentActivity.set(currentActivity);
    }

    private static Activity getCurrentActivity() {
        return ContentUtils.currentActivity.get();
    }

    public static void provideAssets(Activity activity) {
        documentsProvided.clear();
        try {
            for (String document : activity.getAssets().list("models")) {
                documentsProvided.put(document, Uri.parse("assets://assets/models/" + document));
            }
        } catch (IOException ex) {
            Log.e("ContentUtils", "Error listing assets from models folder", ex);
        }
    }

    private static Uri getUri(String name) {
        return documentsProvided.get(name);
    }

    /**
     * Find the relative file that should be already selected by the user
     *
     * @param path relative file
     * @return InputStream of the file
     * @throws IOException if there is an error opening stream
     */
    public static InputStream getInputStream(String path) throws IOException {
        Uri uri = getUri(path);
        if (uri == null && currentDir != null) {
            uri = Uri.parse("file://" + new File(currentDir, path).getAbsolutePath());
        }
        if (uri != null) {
            return getInputStream(uri);
        }
        Log.w("ContentUtils","Media not found: "+path);
        Log.w("ContentUtils","Available media: "+documentsProvided);
        return null;
    }

    static InputStream getInputStream(URI uri) throws IOException {
        return getInputStream(Uri.parse(uri.toString()));
    }

    public static InputStream getInputStream(Uri uri) throws IOException {
        Log.i("ContentUtils", "Opening stream " + uri.getPath());
        if ("assets".equals(uri.getScheme())) {
            Log.i("ContentUtils", "Opening asset: " + uri.getPath());
            return getCurrentActivity().getAssets().open(uri.getPath().substring(1));
        }
        if ("http".equals(uri.getScheme()) || "https".equals(uri.getScheme())) {
            return new URL(uri.toString()).openStream();
        }
        return getCurrentActivity().getContentResolver().openInputStream(uri);
    }

}