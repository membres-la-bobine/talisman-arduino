package org.andresoviedo.android_3d_model_engine.model;

import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import org.andresoviedo.android_3d_model_engine.services.wavefront.WavefrontLoader;
import org.andresoviedo.android_3d_model_engine.services.wavefront.WavefrontLoader.FaceMaterials;
import org.andresoviedo.android_3d_model_engine.services.wavefront.WavefrontLoader.Faces;
import org.andresoviedo.android_3d_model_engine.services.wavefront.WavefrontLoader.Materials;
import org.andresoviedo.android_3d_model_engine.services.wavefront.WavefrontLoader.Tuple3;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the basic 3D data necessary to build the 3D object
 *
 * @author andres
 *
 */
public class Object3DData {

    // opengl version to use to draw this object
    private int version = 5;
    /**
     * The assets directory where the files reside so we can build referenced files in the model like material and
     * textures files
     */
    // private String assetsDir;
    private String id;
    private boolean drawUsingArrays = false;

    // Model data for the simplest object

    private float[] color;
    /**
     * The minimum thing we can draw in space is a vertex (or point).
     * This drawing mode uses the vertexBuffer
     */
    private int drawMode = GLES20.GL_POINTS;

    // Model data
    private FloatBuffer vertexBuffer = null;
    private FloatBuffer vertexNormalsBuffer = null;
    private IntBuffer drawOrderBuffer = null;
    private ShortBuffer shortDrawOrderBuffer = null;  // in case system doesn't support ints
    private ArrayList<Tuple3> texCoords;
    private Faces faces;
    private FaceMaterials faceMats;
    private Materials materials;
    private String textureFile;

    // Processed arrays
    private FloatBuffer vertexArrayBuffer = null;
    private FloatBuffer vertexColorsArrayBuffer = null;
    private FloatBuffer vertexNormalsArrayBuffer = null;
    private FloatBuffer textureCoordsArrayBuffer = null;
    private List<int[]> drawModeList = null;
    private byte[] textureData = null;

    // derived data
    private BoundingBox boundingBox;

    // Transformation data
    protected float[] position = new float[] { 0f, 0f, 0f };
    private float[] rotation = new float[] { 0f, 0f, 0f };
    private float[] scale = new float[] { 1, 1, 1 };
    private float[] modelMatrix = new float[16];

    {
        Matrix.setIdentityM(modelMatrix,0);
    }


    // Async Loader
    private WavefrontLoader.ModelDimensions modelDimensions;
    private WavefrontLoader loader;

    // errors detected
    private List<String> errors = new ArrayList<>();

    public Object3DData(FloatBuffer vertexArrayBuffer) {
        this.vertexArrayBuffer = vertexArrayBuffer;
        this.version = 1;
    }

    public Object3DData(FloatBuffer verts, FloatBuffer normals, ArrayList<Tuple3> texCoords, Faces faces,
                        FaceMaterials faceMats, Materials materials) {
        super();
        this.vertexBuffer = verts;
        this.vertexNormalsBuffer = normals;
        this.texCoords = texCoords;
        this.faces = faces;  // parameter "faces" could be null in case of async loading
        this.faceMats = faceMats;
        this.materials = materials;
    }

    public void setLoader(WavefrontLoader loader) {
        this.loader = loader;
    }


    public WavefrontLoader getLoader() {
        return loader;
    }

    public void setDimensions(WavefrontLoader.ModelDimensions modelDimensions) {
        this.modelDimensions = modelDimensions;
    }

    public boolean isVisible() {
        return true;
    }

    public int getVersion() {
        return version;
    }

    public Object3DData setVersion(int version) {
        this.version = version;
        return this;
    }

    public boolean isChanged() {
        return true;
    }

    public Object3DData setId(String id) {
        this.id = id;
        return this;
    }

    public String getId() {
        return id;
    }

    public float[] getColor() {
        return color;
    }

    public Object3DData setColor(float[] color) {
        // color variable when using single color
        this.color = color;
        return this;
    }

    public int getDrawMode() {
        return drawMode;
    }

    public Object3DData setDrawMode(int drawMode) {
        this.drawMode = drawMode;
        return this;
    }

    // -----------

    public byte[] getTextureData() {
        return textureData;
    }

    public void setTextureData(byte[] textureData) {
        this.textureData = textureData;
    }

    public Object3DData setPosition(float[] position) {
        this.position = position;
        updateModelMatrix();
        return this;
    }

    public float[] getPosition() {
        return position;
    }

    private float getPositionX() {
        return position != null ? position[0] : 0;
    }

    private float getPositionY() {
        return position != null ? position[1] : 0;
    }

    private float getPositionZ() {
        return position != null ? position[2] : 0;
    }

    private float[] getRotation() {
        return rotation;
    }

    private float getRotationZ() {
        return rotation[2];
    }

    public void setScale(float[] scale){
        this.scale = scale;
        updateModelMatrix();
    }

    private float[] getScale(){
        return scale;
    }

    private float getScaleX() {
        return getScale()[0];
    }

    private float getScaleY() {
        return getScale()[1];
    }

    private float getScaleZ() {
        return getScale()[2];
    }

    private void updateModelMatrix(){
        Matrix.setIdentityM(modelMatrix, 0);
        if (getRotation() != null) {
            Matrix.rotateM(modelMatrix, 0, getRotation()[0], 1f, 0f, 0f);
            Matrix.rotateM(modelMatrix, 0, getRotation()[1], 0, 1f, 0f);
            Matrix.rotateM(modelMatrix, 0, getRotationZ(), 0, 0, 1f);
        }
        if (getScale() != null) {
            Matrix.scaleM(modelMatrix, 0, getScaleX(), getScaleY(), getScaleZ());
        }
        if (getPosition() != null) {
            Matrix.translateM(modelMatrix, 0, getPositionX(), getPositionY(), getPositionZ());
        }
    }

    public float[] getModelMatrix(){
        return modelMatrix;
    }

    public IntBuffer getDrawOrder() {
        return drawOrderBuffer;
    }

    /**
     * In case OpenGL doesn't support using GL_UNSIGNED_INT for glDrawElements(), then use this buffer
     * @return the draw buffer as short
     */
    public ShortBuffer getDrawOrderAsShort() {
        if (shortDrawOrderBuffer == null && drawOrderBuffer != null){
            shortDrawOrderBuffer = createNativeByteBuffer(drawOrderBuffer.capacity() * 2).asShortBuffer();
            for (int i=0; i<drawOrderBuffer.capacity(); i++){
                shortDrawOrderBuffer.put((short)drawOrderBuffer.get(i));
            }
        }
        return shortDrawOrderBuffer;
    }

    public Object3DData setDrawOrder(IntBuffer drawBuffer) {
        this.drawOrderBuffer = drawBuffer;
        return this;
    }

    public boolean isDrawUsingArrays() {
        return drawUsingArrays;
    }

    public boolean isFlipTextCoords() {
        return true;
    }

    public void setDrawUsingArrays(boolean drawUsingArrays) {
        this.drawUsingArrays = drawUsingArrays;
    }

    public FloatBuffer getVerts() {
        return vertexBuffer;
    }

    public FloatBuffer getNormals() {
        return vertexNormalsBuffer;
    }

    public ArrayList<Tuple3> getTexCoords() {
        return texCoords;
    }

    public Faces getFaces() {
        return faces;
    }

    public FaceMaterials getFaceMats() {
        return faceMats;
    }

    public Materials getMaterials() {
        return materials;
    }

    // -------------------- Buffers ---------------------- //

    public FloatBuffer getVertexBuffer() {
        return vertexBuffer;
    }

    public FloatBuffer getVertexArrayBuffer() {
        return vertexArrayBuffer;
    }

    public void setVertexArrayBuffer(FloatBuffer vertexArrayBuffer) {
        this.vertexArrayBuffer = vertexArrayBuffer;
    }

    public FloatBuffer getVertexNormalsArrayBuffer() {
        return vertexNormalsArrayBuffer;
    }

    public void setVertexNormalsArrayBuffer(FloatBuffer vertexNormalsArrayBuffer) {
        this.vertexNormalsArrayBuffer = vertexNormalsArrayBuffer;
    }

    public FloatBuffer getTextureCoordsArrayBuffer() {
        return textureCoordsArrayBuffer;
    }

    public void setTextureCoordsArrayBuffer(FloatBuffer textureCoordsArrayBuffer) {
        this.textureCoordsArrayBuffer = textureCoordsArrayBuffer;
    }

    public List<int[]> getDrawModeList() {
        return drawModeList;
    }

    public Object3DData setDrawModeList(List<int[]> drawModeList) {
        this.drawModeList = drawModeList;
        return this;
    }

    public FloatBuffer getVertexColorsArrayBuffer() {
        return vertexColorsArrayBuffer;
    }

    public void setVertexColorsArrayBuffer(FloatBuffer vertexColorsArrayBuffer) {
        this.vertexColorsArrayBuffer = vertexColorsArrayBuffer;
    }

    public void setTextureFile(String textureFile) {
        this.textureFile = textureFile;
    }

    public String getTextureFile(){
        return textureFile;
    }

    private static ByteBuffer createNativeByteBuffer(int length) {
        // initialize vertex byte buffer for shape coordinates
        ByteBuffer bb = ByteBuffer.allocateDirect(length);
        // use the device hardware's native byte order
        bb.order(ByteOrder.nativeOrder());
        return bb;
    }

    public BoundingBox getBoundingBox() {
        FloatBuffer vertexBuffer = getVertexBuffer();
        if (vertexBuffer == null){
            vertexBuffer = getVertexArrayBuffer();
        }
        if (boundingBox == null) {
            boundingBox = BoundingBox.create(getId()+"_BoundingBox", vertexBuffer, getModelMatrix());
        }
        return boundingBox;
    }

    public void centerScale()
        /*
         * Position the model so it's center is at the origin, and scale it so its longest dimension is no bigger than
         * maxSize.
         */
    {
        // calculate a scale factor
        float scaleFactor = 1.0f;
        float largest = modelDimensions.getLargest();
        // System.out.println("Largest dimension: " + largest);
        if (largest != 0.0f)
            scaleFactor = (1.0f / largest);
        Log.i("Object3DData","Scaling model with factor: " + scaleFactor+". Largest: "+largest);

        // get the model's center point
        Tuple3 center = modelDimensions.getCenter();
        Log.i("Object3DData","Objects actual position: " + center.toString());

        // modify the model's vertices
        float x0, y0, z0;
        float x, y, z;
        FloatBuffer vertexBuffer = getVertexBuffer() != null? getVertexBuffer() : getVertexArrayBuffer();
        for (int i = 0; i < vertexBuffer.capacity()/3; i++) {
            x0 = vertexBuffer.get(i*3);
            y0 = vertexBuffer.get(i*3+1);
            z0 = vertexBuffer.get(i*3+2);
            x = (x0 - center.getX()) * scaleFactor;
            vertexBuffer.put(i*3,x);
            y = (y0 - center.getY()) * scaleFactor;
            vertexBuffer.put(i*3+1,y);
            z = (z0 - center.getZ()) * scaleFactor;
            vertexBuffer.put(i*3+2,z);
        }
    } // end of centerScale()

    public void addError(String error) {
        errors.add(error);
    }

    public List<String> getErrors(){
        return errors;
    }
}
