package org.andresoviedo.android_3d_model_engine.drawer;

import android.content.Context;
import android.util.Log;

import org.andresoviedo.android_3d_model_engine.model.Object3D;
import org.andresoviedo.android_3d_model_engine.model.Object3DData;
import org.andresoviedo.util.io.IOUtils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import fr.labobine.talisman.R;

public class DrawerFactory {

    /**
     * shader code loaded from raw resources
     * resources are cached on activity thread
     */
    private Map<String, String> shadersCode = new HashMap<>();
    /**
     * list of opengl drawers
     */
    private Map<String, DrawerImpl> drawers = new HashMap<>();

    public DrawerFactory(Context context) throws IllegalAccessException, IOException {

        Log.i("DrawerFactory", "Discovering shaders...");
        Field[] fields = R.raw.class.getFields();
        for (Field field : fields) {
            String shaderId = field.getName();
            Log.d("DrawerFactory", "Loading shader... " + shaderId);
            int shaderResId = field.getInt(field);
            byte[] shaderBytes = IOUtils.read(context.getResources().openRawResource(shaderResId));
            String shaderCode = new String(shaderBytes);
            shadersCode.put(shaderId, shaderCode);
        }
        Log.i("DrawerFactory", "Shaders loaded: " + shadersCode.size());
    }

    public Object3D getDrawer(Object3DData obj, boolean usingTextures, boolean usingLights, boolean drawColors) {

        // double check features
        boolean isUsingLights = usingLights && (obj.getNormals() != null || obj.getVertexNormalsArrayBuffer() != null);
        boolean isTextured = usingTextures && obj.getTextureData() != null && obj.getTextureCoordsArrayBuffer() != null;
        boolean isColoured = drawColors && obj != null && obj.getVertexColorsArrayBuffer() != null;

        // build shader id according to features

        // get cached drawer
        String shaderId = "shader_" +
                (isUsingLights ? "light_" : "") +
                (isTextured ? "texture_" : "") +
                (isColoured ? "colors_" : "")
                // get cached drawer
                ;
        DrawerImpl drawer = drawers.get(shaderId);
        if (drawer != null) return drawer;

        // build drawer
        String vertexShaderCode = shadersCode.get(shaderId + "vert");
        String fragmentShaderCode = shadersCode.get(shaderId + "frag");
        if (vertexShaderCode == null || fragmentShaderCode == null) {
            Log.e("DrawerFactory", "Shaders not found for " + shaderId);
            return null;
        }

        // experimental: inject glPointSize
        vertexShaderCode = vertexShaderCode.replace("void main(){", "void main(){\n\tgl_PointSize = 5.0;");

        // create drawer
        Log.v("DrawerFactory", "\n---------- Vertex shader ----------\n");
        Log.v("DrawerFactory", vertexShaderCode);
        Log.v("DrawerFactory", "---------- Fragment shader ----------\n");
        Log.v("DrawerFactory", fragmentShaderCode);
        Log.v("DrawerFactory", "-------------------------------------\n");
        drawer = DrawerImpl.getInstance(shaderId, vertexShaderCode, fragmentShaderCode);

        // cache drawer
        drawers.put(shaderId, drawer);

        // return drawer
        return drawer;
    }

    public Object3D getBoundingBoxDrawer() {
        return getDrawer(null, false, false, false);
    }

    public Object3D getPointDrawer() {
        return getDrawer(null, false, false, false);
    }
}
