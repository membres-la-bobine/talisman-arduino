package org.andresoviedo.app.model3D.controller;

import android.opengl.Matrix;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import org.andresoviedo.android_3d_model_engine.model.Camera;
import org.andresoviedo.app.model3D.demo.SceneLoader;
import org.andresoviedo.app.model3D.view.ModelRenderer;
import org.andresoviedo.app.model3D.view.ModelSurfaceView;

import fr.labobine.talisman.HttpCallTask;
import fr.labobine.talisman.ScenarioConfiguration;

public class TouchController {

    private static final String TAG = TouchController.class.getName();

    private final ModelSurfaceView view;
    private final ModelRenderer mRenderer;

    private float x1 = Float.MIN_VALUE;
    private float y1 = Float.MIN_VALUE;
    private float x2 = Float.MIN_VALUE;
    private float y2 = Float.MIN_VALUE;
    private float dx1 = Float.MIN_VALUE;
    private float dy1 = Float.MIN_VALUE;

    private float length = Float.MIN_VALUE;
    private float previousLength = Float.MIN_VALUE;

    private boolean fingersAreClosing = false;
    private boolean isRotating = false;

    private boolean gestureChanged = false;
    private boolean simpleTouch = false;
    private long lastActionTime;
    private int touchDelay = -2;

    private float previousX1;
    private float previousY1;
    private float previousX2;
    private float previousY2;
    private float[] previousVector = new float[4];
    private float[] vector = new float[4];
    private float[] rotationVector = new float[4];


    public TouchController(ModelSurfaceView view, ModelRenderer renderer) {
        super();
        this.view = view;
        this.mRenderer = renderer;
    }

    public synchronized boolean onTouchEvent(MotionEvent motionEvent) {
        // MotionEvent reports input details from the touch screen
        // and other input controls. In this case, you are only
        // interested in events where the touch position changed.

        switch (motionEvent.getActionMasked()) {
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_POINTER_UP:
            case MotionEvent.ACTION_HOVER_EXIT:
            case MotionEvent.ACTION_OUTSIDE:
                // this to handle "1 simple touch"
                if (lastActionTime > SystemClock.uptimeMillis() - 250) {
                    simpleTouch = true;
                } else {
                    gestureChanged = true;
                    touchDelay = 0;
                    lastActionTime = SystemClock.uptimeMillis();
                    simpleTouch = false;
                }
                break;
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN:
            case MotionEvent.ACTION_HOVER_ENTER:
                Log.v(TAG, "Gesture changed...");
                gestureChanged = true;
                touchDelay = 0;
                lastActionTime = SystemClock.uptimeMillis();
                simpleTouch = false;
                break;
            case MotionEvent.ACTION_MOVE:
                simpleTouch = false;
                touchDelay++;
                break;
            default:
                Log.w(TAG, "Unknown state: " + motionEvent.getAction());
                gestureChanged = true;
        }

        int pointerCount = motionEvent.getPointerCount();

        if (pointerCount == 1) {
            x1 = motionEvent.getX();
            y1 = motionEvent.getY();
            if (gestureChanged) {
                Log.v(TAG, "x:" + x1 + ",y:" + y1);
                previousX1 = x1;
                previousY1 = y1;
            }
            dx1 = x1 - previousX1;
            dy1 = y1 - previousY1;
        } else if (pointerCount == 2) {
            x1 = motionEvent.getX(0);
            y1 = motionEvent.getY(0);
            x2 = motionEvent.getX(1);
            y2 = motionEvent.getY(1);
            vector[0] = x2 - x1;
            vector[1] = y2 - y1;
            vector[2] = 0;
            vector[3] = 1;
            float len = Matrix.length(vector[0], vector[1], vector[2]);
            vector[0] /= len;
            vector[1] /= len;

            // Log.v(TAG, "x1:" + x1 + ",y1:" + y1 + ",x2:" + x2 + ",y2:" + y2);
            if (gestureChanged) {
                previousX1 = x1;
                previousY1 = y1;
                previousX2 = x2;
                previousY2 = y2;
                System.arraycopy(vector, 0, previousVector, 0, vector.length);
            }
            dx1 = x1 - previousX1;
            dy1 = y1 - previousY1;
            float dx2 = x2 - previousX2;
            float dy2 = y2 - previousY2;

            rotationVector[0] = (previousVector[1] * vector[2]) - (previousVector[2] * vector[1]);
            rotationVector[1] = (previousVector[2] * vector[0]) - (previousVector[0] * vector[2]);
            rotationVector[2] = (previousVector[0] * vector[1]) - (previousVector[1] * vector[0]);
            len = Matrix.length(rotationVector[0], rotationVector[1], rotationVector[2]);
            rotationVector[0] /= len;
            rotationVector[1] /= len;
            rotationVector[2] /= len;

            previousLength = (float) Math
                    .sqrt(Math.pow(previousX2 - previousX1, 2) + Math.pow(previousY2 - previousY1, 2));
            length = (float) Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));

            // gesture detection
            boolean isOneFixedAndOneMoving = ((dx1 + dy1) == 0) != (((dx2 + dy2) == 0));
            fingersAreClosing = !isOneFixedAndOneMoving && (Math.abs(dx1 + dx2) < 10 && Math.abs(dy1 + dy2) < 10);
            isRotating = !isOneFixedAndOneMoving && (dx1 != 0 && dy1 != 0 && dx2 != 0 && dy2 != 0)
                    && rotationVector[2] != 0;
        }

        if (pointerCount == 1 && simpleTouch) {
            SceneLoader scene = view.getModelActivity().getScene();
            int objectTouched = scene.processTouch(x1,y1);

            if (objectTouched >= 0) {
                Log.v(TAG, "Sent HTTP request ! Object detected : " + objectTouched);
                HttpCallTask task = new HttpCallTask();

                ScenarioConfiguration.scenario = objectTouched;
                try {
                    int resultCode = task.execute("http://192.168.4.1:8888/command/").get();
                    if (resultCode != 200) {
                        throw new Exception("Bad response code : " + resultCode);
                    }
                } catch (Exception e) {
                    Toast.makeText(view.getContext(), "HTTP call error" + e.getMessage(), Toast.LENGTH_LONG).show();
                }

                if (task.getException() != null) {
                    Toast.makeText(view.getContext(), "HTTP call error" + task.getException().getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }

        int max = Math.max(mRenderer.getWidth(), mRenderer.getHeight());
        if (touchDelay > 1) {
            // INFO: Process gesture
            SceneLoader scene = view.getModelActivity().getScene();
            Camera camera = scene.getCamera();
            if (pointerCount == 1) {
                // Log.v(TAG, "Translating camera (dx,dy) '" + dx1 + "','" + dy1 + "'...");
                dx1 = (float)(dx1 / max * Math.PI * 2);
                dy1 = (float)(dy1 / max * Math.PI * 2);
                camera.translateCamera(dx1,dy1);
            } else if (pointerCount == 2) {
                if (fingersAreClosing) {
                    float zoomFactor = (length - previousLength) / max * mRenderer.getFar();
                    Log.v(TAG, "Zooming '" + zoomFactor + "'...");
                    camera.MoveCameraZ(zoomFactor);
                }
                if (isRotating) {
                    Log.v(TAG, "Rotating camera '" + Math.signum(rotationVector[2]) + "'...");
                    camera.Rotate((float) (Math.signum(rotationVector[2]) / Math.PI) / 4);
                }
            }
        }

        previousX1 = x1;
        previousY1 = y1;
        previousX2 = x2;
        previousY2 = y2;

        System.arraycopy(vector, 0, previousVector, 0, vector.length);

        if (gestureChanged && touchDelay > 1) {
            gestureChanged = false;
            Log.v(TAG, "Fin");
        }

        view.requestRender();
        return true;
    }
}
