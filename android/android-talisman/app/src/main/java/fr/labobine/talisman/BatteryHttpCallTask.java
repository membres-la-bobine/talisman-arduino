package fr.labobine.talisman;

import android.os.AsyncTask;
import android.util.Log;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class BatteryHttpCallTask extends AsyncTask<String, Void, Integer> {
    private Exception exception = null;

    protected Integer doInBackground(String... urls) {
        try {
            OkHttpClient client = new OkHttpClient();

            HttpUrl.Builder urlBuilder = HttpUrl.parse(urls[0]).newBuilder();

            urlBuilder.addQueryParameter("batterie", ScenarioConfiguration.battery ? "on" : "off");

            String url = urlBuilder.build().toString();
            Log.i("HttpCallTask", "Call url : " + url) ;
            Request request = new Request.Builder().url(url).build();
            Response response = client.newCall(request).execute();
            return response.code();
        } catch (Exception e) {
            this.exception = e;
        }
        return -1;
    }

    public Exception getException() {
        return exception;
    }
}