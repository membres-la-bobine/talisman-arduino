# Renomme copie et renomme le fichier spiffs.bin
Import("env", "projenv")
#import os
from shutil import copyfile
import datetime
# access to global construction environment
#print(env)
# access to project build environment (is used source files in "src" folder)
#print(projenv)
def renameSpiffsbin(*args, **kwargs):
    date_tag = datetime.datetime.now().strftime("%Y")+datetime.datetime.now().strftime("%m")+datetime.datetime.now().strftime("%d")
    target = str(kwargs['target'][0])
    #print (target)
    copyfile(target, "../bin/%s_WebServer_spiffs.bin" %date_tag)

def renameFirmwarebin(*args, **kwargs):
    date_tag = datetime.datetime.now().strftime("%Y")+datetime.datetime.now().strftime("%m")+datetime.datetime.now().strftime("%d")
    target = str(kwargs['target'][0])
    #print (target)
    copyfile(target, "../bin/%s_WebServer_firmware.bin" %date_tag)
    
env.AddPostAction("$BUILD_DIR/spiffs.bin", renameSpiffsbin)
env.AddPostAction("$BUILD_DIR/firmware.bin", renameFirmwarebin)