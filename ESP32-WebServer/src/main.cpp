/* Projet Talisman
  * Une petite appli pour gérer la communication avec la tablette et les rubans de leds
  *
  * @author 2020 Jean-Louis Frucot <frucot.jeanlouis@free.fr>
  * @see The GNU Public License (GPL)
  *
  * this program is free software you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY
  * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  * for more details.
  *
  * You should have received a copy of the GNU General Public License along
  * with this program. If not, see <http://www.gnu.org/licenses/>.
  */

#include "headers.h"
#include "scenarios.h"
#include "server.h" // Initialise les callback en fonction de l'URL reçue

AsyncWebServer server(SERVER_PORT); // On déclare un serveur sur le port defini dans settings.h
// EasyOTA OTA("Talisman"); 
void setup()
{
  ////////////////////////    Initialisation des ports série   //////////////////////////////////
  Serial.begin(9600);
  m_serial.begin(9600, SERIAL_8N1, RXPIN, TXPIN);
  delay(200); // On laisse un peu de temps pour que les choses se mettent en place
  ////////////////////////    Paramètrage du serveur en tant que Access Point ///////////////////
  bool ok = WiFi.softAP(ssid, password, 1); // On fixe les IP
#if OUTPUT_SERIAL_PRINT
  Serial.print("Setting soft-AP ... ");
  Serial.println(ok ? "OK" : "Échoué!"); // Création du Point d'accès
#endif
  delay(2000);                               // Pour que tout se passe bien
  WiFi.softAPConfig(local, gateway, subnet); // On fixe les IP
#if OUTPUT_SERIAL_PRINT
  Serial.print("Soft-AP IP address = ");
  Serial.println(WiFi.softAPIP());
#endif

  /////////////////////////// Création des scénarios   ///////////////////////////////////////////
  create_animations();

  /////////////////////// Initialize SPIFFS pour stoker les fichiers html, css, js... //////////
  if (!SPIFFS.begin(true))
  {
#if OUTPUT_SERIAL_PRINT
    Serial.println("Une erreur et survenue lors du montage SPIFFS");
#endif
    return;
  }

  initServer(server); // On initialise les callback en fonction des URL reçues

  AsyncElegantOTA.begin(&server); // Démarre ElegantOTA à l'adresse /update/
  // clearAllRubans();
  server.begin();
}

void loop()
{
  AsyncElegantOTA.loop();
  while (m_serial.available())
  {
    m_serial.readStringUntil('<');                  // le signe '<' n'est pas compris dans la chaine.
    String message = m_serial.readStringUntil('>'); // le signe '>' n'est pas compris dans la chaine.
    message.replace("<", "");
    message.replace(">", "");

    /** On obtient un message de la forme numRuban:numAction (ie 0:1)
     * Un message est de la forme <numSegment:numAction>
     * un certain nombre de codes ont une signification particulière :
     * 99 pour éteindre tous les segments
     * 98 pour allumer le wifi de l'autre ESP32
     * 90 une étape est terminée
     * 91 un segment a fini son animation
     **/

    if (message.indexOf("90") != -1)
    {

      Serial.print("Message reçu : ");
      Serial.print(message);
      Serial.print(" index de 90 : ");
      Serial.println(message.indexOf("90"));
      for (uint8_t i = 0; i < nb_scenarios; i++)
      {
        if (tab_scenarios[i].scen_playingStatus != stopped)
        {
          m_serial.println(anims_nextStep(i)); // On envoie le nouveau segment à animer
        }
      }
    }
  }
}

void clearAllRubans()
{
  String message = String("<" + String(99) + ":" + String(0) + ">");
  m_serial.println(message);
  m_serial.flush();
#if OUTPUT_SERIAL_PRINT
  Serial.println(message);
#endif
}

void startWifiRuban()
{
  String message = String("<" + String(98) + ":" + String(1) + ">");
  m_serial.println(message);
#if OUTPUT_SERIAL_PRINT
  Serial.println(message);
#endif
}
