#include <Arduino.h>
#include <list>
#include <iostream>
#include <algorithm>
using namespace std;
#define OUTPUT_SERIAL_PRINT 1
enum SegmentName : uint8_t
{
    frontalsensor,
    portesensor,
    montantsensor,
    montantline,
    pretensconducteur,
    pretenspassager,
    pretenspassagerline,
    airbagconducteur,
    airbagpassager,
    airbagpassagerline,
    airbagrideauline,
    airbagrideau,
    unitecentrale
};

enum num_scenarios : uint8_t
{
    scen_init_test = 0,
    scen_front_1,
    scen_front_2,
    scen_lateral_1,
    scen_lateral_2
};
enum playingStatus : uint8_t
{
    stopped = 0,
    running = 1,
    paused = 2
};
enum modeAnimation : uint8_t
{
    manuelle = 0,
    automatique = 1
};
struct animation
{
    uint8_t scen_playingStatus = stopped; // l'animation est-elle activée ou non ?
    list<uint8_t> list_Steps;
    list<uint8_t>::const_iterator it_list_Steps;
};

const uint8_t nb_scenarios = 5;
animation tab_scenarios[nb_scenarios];
void create_animations()
{
    //// scenario de test : on allume tous les rubans l'un après l'autre
    for (uint8_t i = frontalsensor; i < unitecentrale + 1; i++)
    {
        tab_scenarios[scen_init_test].list_Steps.push_back(i);
        tab_scenarios[scen_init_test].list_Steps.push_back(255); // 255 marque la séparation entre deux étapes du sccénario
    }

    //// scénario choc frontal conducteur uniquement
    tab_scenarios[scen_front_1].list_Steps.push_back(frontalsensor);
    tab_scenarios[scen_front_1].list_Steps.push_back(255);
    tab_scenarios[scen_front_1].list_Steps.push_back(pretensconducteur);
    tab_scenarios[scen_front_1].list_Steps.push_back(255);
    tab_scenarios[scen_front_1].list_Steps.push_back(airbagconducteur);
    tab_scenarios[scen_front_1].list_Steps.push_back(255);

    //// scénario choc frontal conducteur et passager
    tab_scenarios[scen_front_2].list_Steps.push_back(frontalsensor);
    tab_scenarios[scen_front_2].list_Steps.push_back(255);
    tab_scenarios[scen_front_2].list_Steps.push_back(pretenspassagerline);
    tab_scenarios[scen_front_2].list_Steps.push_back(255);
    tab_scenarios[scen_front_2].list_Steps.push_back(pretensconducteur);
    tab_scenarios[scen_front_2].list_Steps.push_back(pretenspassager);
    tab_scenarios[scen_front_2].list_Steps.push_back(255);
    tab_scenarios[scen_front_2].list_Steps.push_back(airbagconducteur);
    tab_scenarios[scen_front_2].list_Steps.push_back(airbagpassager);
    tab_scenarios[scen_front_2].list_Steps.push_back(255);

    //// Choc latéral conducteur seul
    tab_scenarios[scen_lateral_1].list_Steps.push_back(portesensor);
    tab_scenarios[scen_lateral_1].list_Steps.push_back(montantsensor);
    tab_scenarios[scen_lateral_1].list_Steps.push_back(255);
    tab_scenarios[scen_lateral_1].list_Steps.push_back(montantline);
    tab_scenarios[scen_lateral_1].list_Steps.push_back(255);
    tab_scenarios[scen_lateral_1].list_Steps.push_back(pretensconducteur);
    tab_scenarios[scen_lateral_1].list_Steps.push_back(255);
    tab_scenarios[scen_lateral_1].list_Steps.push_back(airbagrideauline);
    tab_scenarios[scen_lateral_1].list_Steps.push_back(255);
    tab_scenarios[scen_lateral_1].list_Steps.push_back(airbagrideau);
    tab_scenarios[scen_lateral_1].list_Steps.push_back(255);

        //// Choc latéral conducteur et passager
    tab_scenarios[scen_lateral_2].list_Steps.push_back(portesensor);
    tab_scenarios[scen_lateral_2].list_Steps.push_back(montantsensor);
    tab_scenarios[scen_lateral_2].list_Steps.push_back(255);
    tab_scenarios[scen_lateral_2].list_Steps.push_back(montantline);
    tab_scenarios[scen_lateral_2].list_Steps.push_back(255);
    tab_scenarios[scen_lateral_2].list_Steps.push_back(pretenspassagerline);
    tab_scenarios[scen_lateral_2].list_Steps.push_back(255);
    tab_scenarios[scen_lateral_2].list_Steps.push_back(pretensconducteur);
    tab_scenarios[scen_lateral_2].list_Steps.push_back(pretenspassager);
    tab_scenarios[scen_lateral_2].list_Steps.push_back(255);
    tab_scenarios[scen_lateral_2].list_Steps.push_back(airbagrideauline);
    tab_scenarios[scen_lateral_2].list_Steps.push_back(255);
    tab_scenarios[scen_lateral_2].list_Steps.push_back(airbagrideau);
    tab_scenarios[scen_lateral_2].list_Steps.push_back(255);
}

String anims_nextStep(uint8_t numScenario)
{
    String message = "";

#if OUTPUT_SERIAL_PRINT
    Serial.print("numScenario : ");
    Serial.println(numScenario);
#endif
    // if (tab_scenarios[numScenario].scen_playingStatus == paused && animAuto == manuelle)
    // {
    if (tab_scenarios[numScenario].scen_playingStatus == stopped) // il n'y a rien à faire
    {
#if OUTPUT_SERIAL_PRINT
        Serial.println("anim_NextStep : stopped");
#endif
        return message;
    }
    else
    {
        // Si on est à la fin de la liste des segments à animer, on arrête
        if (tab_scenarios[numScenario].it_list_Steps == tab_scenarios[numScenario].list_Steps.end())
        {
            tab_scenarios[numScenario].scen_playingStatus = stopped; // Indique que le scenario est arrêté (on a fini l'animation)
#if OUTPUT_SERIAL_PRINT
            Serial.println("Stopped");
#endif
        }
        // Sinon on continue à envoyer la liste des segments à animer
        else
        {

            if (*tab_scenarios[numScenario].it_list_Steps < 255) // C'est peut-être un code valide !!!
            {
                do
                {
                    message = message + String("<" + String(*tab_scenarios[numScenario].it_list_Steps) + ":1>");
                    ++tab_scenarios[numScenario].it_list_Steps;
                } while (*tab_scenarios[numScenario].it_list_Steps < 255); // Tant qu'on est pas sur un séparateur
                                                                           // tab_scenarios[numScenario].scen_playingStatus = paused;
                                                                           // On va pointer sur le prochain segment à animer
#if OUTPUT_SERIAL_PRINT
                Serial.print("Segment n°");
                Serial.print(*tab_scenarios[numScenario].it_list_Steps);
                Serial.print(" Playing Status : ");
                Serial.println(tab_scenarios[numScenario].scen_playingStatus);
#endif
                ++tab_scenarios[numScenario].it_list_Steps; // Et on avance d'un cran dans la liste
            }
        }
    }
#if OUTPUT_SERIAL_PRINT
    Serial.println(message);
#endif
    // }
    return message;
}

String startScenario(uint8_t numScenario)
{
    tab_scenarios[numScenario].scen_playingStatus = running;
    tab_scenarios[numScenario].it_list_Steps = tab_scenarios[numScenario].list_Steps.begin();
    Serial.println("Scenario Started");
    return anims_nextStep(numScenario);
}