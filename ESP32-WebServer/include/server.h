#ifndef SERVER_H
#define SERVER_H

#include "headers.h"
#include "settings.h"

#define OUTPUT_SERIAL_PRINT 1 // 1 affiche les debugs, 0 ne les affiche pas

/** \fn initServer(AsyncWebServer &server)
 * \brief Initialise les callbacks du serveur
 * 
 * \param &server Référence vers le serveur créé dans main.cpp
 */
void initServer(AsyncWebServer &server)
{
  /* Page d'accueil du site talisman */
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(SPIFFS, "/index.html", String(), false);
  });

  /* Si l'adresse contient wifiruban, on demande le démarrage du serveur de l'ESP pilotant les rubans de LEDs */
  server.on("/wifiruban/", HTTP_GET, [](AsyncWebServerRequest *request) {
    startWifiRuban();
    request->send(SPIFFS, "/index.html", String(), false);
  });
  /* Page des boutons de commande des rubans */
  server.on("/command/", HTTP_GET, [](AsyncWebServerRequest *request) {

#if OUTPUT_SERIAL_PRINT
    Serial.println(request->url());
    //List all parameters
    int params = request->params();
    for(int i=0;i<params;i++)
    {
      AsyncWebParameter* p = request->getParam(i);
      Serial.printf("POST[%s]: %s\n", p->name().c_str(), p->value().c_str());
    }
#endif
    
    // first handle passager and battery params as they can modify scenario behaviour
    if (request->hasParam("passager"))
    {
      passenger_number = request->getParam("passager")->value().toInt();
      if (passenger_number == 2)
      {
        m_serial.println("<85:1>");
      }
      else
      {
        m_serial.println("<85:0>");
      }
    }

    if (request->hasParam("batterie"))
    {
      String batteryValue = request->getParam("batterie")->value();
      battery_state = batteryValue == "on" ? true : false;
      if (battery_state)
      {
        m_serial.println("<80:1>");
      }
      else
      {
        m_serial.println("<80:0>");
      }
    }

    if (request->hasParam("scenario"))                          // si on a ce paramètre dans l'url
    {                                                           //
      AsyncWebParameter *param = request->getParam("scenario"); // On récupère la valeur du paramètre ruban
      uint8_t scenarioNumber = param->value().toInt();              // On convertit le paramètre en int

      // si on a un passager on modifie le numero du scenario
      if (passenger_number == 2 ) {
        Serial.println("Change scenario number because there is 2 passengers !");
        if (scenarioNumber == scen_front_1)
          scenarioNumber = scen_front_2;
        if (scenarioNumber == scen_lateral_1)
          scenarioNumber = scen_lateral_2;
      }

      m_serial.println(startScenario(scenarioNumber));              // On envoie le code retourné par startScenario à l'autre ESP32
      Serial.printf("Start Scenario n°%d\n" ,scenarioNumber);
    }

    bool tablette = false;

    if (request->hasParam("tablette"))
    {
      if (request->getParam("tablette")->value().toInt() == 1)
      {
        tablette = true;
      }
    }

    // send response
    if (tablette)
    {
      request->send(200, "text/plain", "");
    }
    else
    {
      request->send(SPIFFS, "/talisman.html", String(), false, processor); // Et on retourne la page mise à jour par le "processor"
    }
  });

  server.onNotFound(notFound);
}

/** \fn processor(const String &var)
 * \brief Parse le fichier html retourné et remplace les balises par la bonne valeur
 * 
 * \param &var La chaine à modifier
 * \return La chaine modifiée
 */
String processor(const String &var) // Permet de modifier la page de retour talisman.html
{
  return String();
}

/** \fn notFound(AsyncWebServerRequest *request)
 * \brief Retourne une page 404 en cas de page non trouvée
 * 
 * \param *request Pointeur vers la requette à l'origine du problème
 */
void notFound(AsyncWebServerRequest *request)
{
  request->send(404, "text/plain", "Page non trouvée");
#if OUTPUT_SERIAL_PRINT
  Serial.println("Page non trouvée");
#endif
}
#endif // SERVER_H