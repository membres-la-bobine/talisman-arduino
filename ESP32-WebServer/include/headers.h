#ifndef HEADERS_H
#define HEADERS_H

#include <Arduino.h>
#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
#include <FS.h>
#include <SPIFFS.h>


#include "settings.h"


/** Enumération des statuts possibles pour les rubans **/
enum Status : uint8_t
{
  OFF = 0,
  ON = 1
};
/** On crée une liaison série entre les deux ESP **/
HardwareSerial m_serial(1);
/** Mode d'execution des scenarios : false : mode pas à pas, true : mode automatique **/
bool animAuto = true;
/** Nombre de passager **/
int passenger_number = 1;
/** Etat de la batterie **/
boolean battery_state = false;
/** On demande à passer à l'étape suivante de l'animation **/
bool nextStepResqueted = false;
/** callback gérant une page non trouvée **/
void notFound(AsyncWebServerRequest *request);
/** Demande le démarrage du wifi pour le deuxième ESP **/
void startWifiRuban();
/** Demande l'inversion de l'état du ruban passé en paramètre : running/stopped **/
void toggleStrip(uint8_t numRuban);
/** Demande l'extinction de tous les rubans **/
void clearAllRubans();
/** Met à jour le statut du ruban passé dans message **/
void updateStripStatus(String message);
/** callback permettant de modifier la page html retournée **/
String processor(const String &var);
#endif // HEADER_H